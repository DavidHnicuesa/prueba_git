﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Realizar un programa que solicite ingresar dos números distintos y muestre por pantalla el mayor de ellos.
namespace EstructuraCondicionalCompuesta1
{
    class CondicionalCompuesto
    {
        static void Main(string[] args)
        {
            int num1, num2;
            string linea;
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.Write("Ingrese segundo valor:");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            if (num1 > num2)
            {
                Console.Write(num1);
            }
            else
            {
                Console.Write(num2);
            }
            Console.ReadKey();
        }
    }
}
/*Ejercicios:
 * 1.- Realizar un programa que lea por teclado dos números, si el primero es mayor al segundo informar su suma y diferencia, en caso contrario informar el producto y la división del primero respecto al segundo. 
 * 2.- Se ingresa por teclado un número positivo de uno o dos dígitos (1..99) mostrar un mensaje indicando si el número tiene uno o dos dígitos.
(Tener en cuenta que condición debe cumplirse para tener dos dígitos, un número entero) 
 



















 1.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraCondicionalCompuesta2
{
    class Program
    {
        static void Main(string[] args)
        {
    	    int num1,num2;
            string linea;
    	    Console.Write("Ingrese primer valor:");
            linea=Console.ReadLine();
    	    num1=int.Parse(linea);
    	    Console.Write("Ingrese segundo valor:");
            linea=Console.ReadLine();
    	    num2=int.Parse(linea);
    	    if (num1>num2)
            {
    	        int suma,diferencia;
    	        suma=num1 + num2;
    	        diferencia=num1 - num2;
    	        Console.Write("La suma de los dos valores es:");
    	        Console.WriteLine(suma);
    	        Console.Write("La diferencia de los dos valores es:");
    	        Console.WriteLine(diferencia);
    	    } 
            else 
            {
    	        int producto,division;
    	        producto=num1 * num2;
    	        division=num1 / num2;
    	        Console.Write("El producto de los dos valores es:");
    	        Console.WriteLine(producto);
    	        Console.Write("La división de los dos valores es:");
    	        Console.WriteLine(division);    		
    	    }
            Console.ReadKey();
        }
    }
}


2.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraCondicionalCompuesta3
{
    class Program
    {
        static void Main(string[] args)
        {
    	    int num;
            string linea;
    	    Console.Write("Ingrese un valor entero de 1 o 2 dígitos:");
            linea=Console.ReadLine();
    	    num=int.Parse(linea);
    	    if (num<10) 
            {
    	        Console.Write("Tiene un dígito");
    	    }
            else 
            {
    	        Console.Write("Tiene dos dígitos");
    	    }
            Console.ReadKey();
        }
    }
}

 
 
 
 */