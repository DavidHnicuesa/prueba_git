﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*Realizar un programa que imprima en pantalla los números del 1 al 100.
Sin conocer las estructuras repetitivas podemos resolver el problema empleando una estructura secuencial. Inicializamos una variable con el valor 1, luego imprimimos la variable, incrementamos nuevamente la variable y así sucesivamente.*/

namespace While
{
    class While
    {
        static void Main(string[] args)
        {
            //Declaramos la variable integer.
            int x;
            //La primera operación inicializa la variable x en 1
            x = 1;
            //Comienza la estructura repetitiva while y disponemos la siguiente condición ( x <= 100), se lee MIENTRAS la variable x sea menor o igual a 100.
            while (x <= 100)
            {
                //Escribe el número.
                Console.Write(x);
                //Escribe los espacios y el guión.
                Console.Write(" - ");
                //Suma 1 a la variable y lo vuelve introducir en la variable.
                x = x + 1;
            }
            Console.ReadKey();
        }
    }
}
/*
Ejercicios:
 1.- Probemos algunas modificaciones de este programa y veamos que cambios se deberían hacer para:
        1 - Imprimir los números del 1 al 500.
        2 - Imprimir los números del 50 al 100.
        3 - Imprimir los números del -50 al 0.
        4 - Imprimir los números del 2 al 100 pero de 2 en 2 (2,4,6,8 ....100).

2.- Escribir un programa que solicite la carga de un valor positivo y nos muestre desde 1 hasta el valor ingresado de uno en uno.
Ejemplo: Si ingresamos 30 se debe mostrar en pantalla los números del 1 al 30.

Es de FUNDAMENTAL importancia analizar los diagramas de flujo y la posterior codificación en C# de los siguientes problemas, en varios problemas se presentan otras situaciones no vistas en el ejercicio anterior.

3.- Desarrollar un programa que permita la carga de 10 valores por teclado y nos muestre posteriormente la suma de los valores ingresados y su promedio.

4.- Una planta que fabrica perfiles de hierro posee un lote de n piezas.
Confeccionar un programa que pida ingresar por teclado la cantidad de piezas a procesar y luego ingrese la longitud de cada perfil; sabiendo que la pieza cuya longitud esté comprendida en el rango de 1,20 y 1,30 son aptas. Imprimir por pantalla la cantidad de piezas aptas que hay en el lote. 

























Solución:
1.-     1 - Debemos cambiar la condición del while con x<=500.
        2 - Debemos inicializar x con el valor 50.
        3 - Inicializar x con el valor -50 y fijar la condición x<=0.
        4 - Inicializar a x con el valor 2 y dentro del bloque repetitivo incrementar a x en 2 
            ( x = x + 2 )

2.- using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaWhile2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n,x;
            string linea;
            Console.Write("Ingrese el valor final:");
            linea=Console.ReadLine();
            n=int.Parse(linea);
            x=1;
            while (x<=n) 
            {
                Console.Write(x);
                Console.Write(" - ");
                x = x + 1;
            }
            Console.ReadKey();
        }
    }
}

3.- using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaWhile3
{
    class Program
    {
        static void Main(string[] args)
        {
            int x,suma,valor,promedio;
            string linea;
            x=1;
            suma=0;
            while (x<=10) 
            {
                Console.Write("Ingrese un valor:");
                linea = Console.ReadLine();
                valor=int.Parse(linea);
                suma=suma+valor;
                x=x+1;
            }
            promedio=suma/10;
            Console.Write("La suma de los 10 valores es:");
            Console.WriteLine(suma);
            Console.Write("El promedio es:");
            Console.Write(promedio);
            Console.ReadKey();
        }
    }
}

4.- using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaWhile4
{
    class Program
    {
        static void Main(string[] args)
        {
            int x,cantidad,n;
            float largo;
            string linea;
            x=1;
            cantidad=0;
            Console.Write("Cuantas piezar procesará:");
            linea = Console.ReadLine();
            n=int.Parse(linea);
            while (x<=n) 
            {
                Console.Write("Ingrese la medida de la pieza:");
                linea = Console.ReadLine();
                largo=float.Parse(linea);
                if (largo>=1.20 && largo<=1.30) 
                {
                    cantidad = cantidad +1;
                }
                x=x + 1;
            }
            Console.Write("La cantidad de piezas aptas son:");
            Console.Write(cantidad);
            Console.ReadKey();
        }
    }
}
 



El tiempo a dedicar a esta sección EJERCICIOS PROPUESTOS debe ser mucho mayor que el empleado a la sección de EJERCICIOS RESUELTOS.
La experiencia dice que debemos dedicar el 80% del tiempo a la resolución individual de problemas y el otro 20% al análisis y codificación de problemas ya resueltos por otras personas.
Es de vital importancia para llegar a ser un buen PROGRAMADOR poder resolver problemas en forma individual.

1.- Escribir un programa que solicite ingresar 10 notas de alumnos y nos informe cuántos tienen notas mayores o iguales a 7 y cuántos menores.

2.- Se ingresan un conjunto de n alturas de personas por teclado. Mostrar la altura promedio de las personas.

3.- En una empresa trabajan n empleados cuyos sueldos oscilan entre $100 y $500, realizar un programa que lea los sueldos que cobra cada empleado e informe cuántos empleados cobran entre $100 y $300 y cuántos cobran más de $300. Además el programa deberá informar el importe que gasta la empresa en sueldos al personal.

4.- Realizar un programa que imprima 25 términos de la serie 11 - 22 - 33 - 44, etc. (No se ingresan valores por teclado)

5.- Mostrar los múltiplos de 8 hasta el valor 500. Debe aparecer en pantalla 8 - 16 - 24, etc.

6.- Realizar un programa que permita cargar dos listas de 15 valores cada una. Informar con un mensaje cual de las dos listas tiene un valor acumulado mayor (mensajes "Lista 1 mayor", "Lista 2 mayor", "Listas iguales")
    Tener en cuenta que puede haber dos o más estructuras repetitivas en un algoritmo.

7.- Desarrollar un programa que permita cargar n números enteros y luego nos informe cuántos valores fueron pares y cuántos impares.
    Emplear el operador “%” en la condición de la estructura condicional:

    	if (valor % 2 == 0)         //Si el if da verdadero luego es par.












1.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaWhile5
{
    class Program
    {
        static void Main(string[] args)
        {
            int x,nota,conta1,conta2;
            string linea;
            x=1;
            conta1=0;
            conta2=0;
            while (x<=10) 
            {
                Console.Write("Ingrese nota:");
                linea = Console.ReadLine();
                nota=int.Parse(linea);
                if (nota>=7) 
                {
                    conta1=conta1 + 1;
                }
                else 
                {
                    conta2=conta2 + 1;
                }
                x=x + 1;
            }
            Console.Write("Cantidad de alumnos con notas mayores o iguales a 7:");
            Console.WriteLine(conta1);
            Console.Write("Cantidad de alumons con notas menores a 7:");
            Console.Write(conta2);
            Console.ReadKey();
        }
    }
}



2.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaWhile6
{
    class Program
    {
        static void Main(string[] args)
        {
            int n,x;
            float altura,suma,promedio;
            string linea;
            Console.Write("Cuantas personas hay:");
            linea = Console.ReadLine();
            n=int.Parse(linea);
            x=1;
            suma=0;
            while (x<=n) 
            {
                Console.Write("Ingrese la altura:");
                linea = Console.ReadLine();
                altura=float.Parse(linea);
                suma=suma + altura;
                x=x + 1;
            }
            promedio=suma/n;
            Console.Write("Altura promedio:");
            Console.Write(promedio);
            Console.ReadKey();
        }
    }
}



3.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaWhile7
{
    class Program
    {
        static void Main(string[] args)
        {
            int n,x,conta1,conta2;
            float sueldo,gastos;
            string linea;
            Console.Write("Cuantos empleados tiene la empresa:");
            linea = Console.ReadLine();
            n=int.Parse(linea);
            x=1;
            conta1=0;
            conta2=0;
            gastos=0;
            while (x<=n) {
                Console.Write("Ingrese el sueldo del empleado:");
                linea = Console.ReadLine();
                sueldo=float.Parse(linea);
                if (sueldo<=300) {
                    conta1=conta1 + 1;
                } else {
            	    conta2=conta2 + 1;
                }
                gastos=gastos+sueldo;
                x=x + 1;
            }
            Console.Write("Cantidad de empleados con sueldos entre 100 y 300:");
            Console.WriteLine(conta1);
            Console.Write("Cantidad de empleados con sueldos mayor a 300:");
            Console.WriteLine(conta2);
            Console.Write("Gastos total de la empresa en sueldos:");
            Console.WriteLine(gastos);
            Console.ReadKey();
        }
    }
}



4.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaWhile8
{
    class Program
    {
        static void Main(string[] args)
        {
            int x,termino;
            x=1;
            termino=11;
            while (x<=25) 
            {
                Console.Write(termino);
                Console.Write(" - ");
                x=x + 1;
                termino=termino + 11;
            }
            Console.ReadKey();
        }
    }
}


5.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaWhile9
{
    class Program
    {
        static void Main(string[] args)
        {
            int mult8;
            mult8=8;
            while (mult8<=500) 
            {
                Console.Write(mult8);
                Console.Write(" - ");
                mult8=mult8 + 8;
            }
            Console.ReadKey();
        }
    }
}



6.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaWhile10
{
    class Program
    {
        static void Main(string[] args)
        {
            int valor,x,suma1,suma2;
            string linea;
            x=1;
            suma1=0;
            suma2=0;
            Console.Write("Primer lista");
            while (x<=15) 
            {
                Console.Write("Ingrese valor:");
                linea = Console.ReadLine();
                valor=int.Parse(linea);
                suma1=suma1 + valor;
                x=x + 1;
            }
            Console.Write("Segunda lista");
            x=1;
            while (x<=15) 
            {
                Console.Write("Ingrese valor:");
                linea = Console.ReadLine();
                valor=int.Parse(linea);
                suma2=suma2 + valor;
                x=x + 1;
            }
            if (suma1>suma2) 
            {
                Console.Write("Lista 1 mayor.");
            }
            else
            {
                if (suma2>suma1)
                {
                    Console.Write("Lista2 mayor.");
                }
                else 
                {
                    Console.Write("Listas iguales.");
                }
            }
            Console.ReadKey();
        }
    }
}




7.-
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaWhile11
{
    class Program
    {
        static void Main(string[] args)
        {
            int n,x,valor,pares,impares;
            string linea;
            x=1;
            pares=0;
            impares=0;
            Console.Write("Cuantos números ingresará:");
            linea = Console.ReadLine();
            n=int.Parse(linea);
            while (x<=n) {
                Console.Write("Ingrese el valor:");
                linea = Console.ReadLine();
                valor = int.Parse(linea); ;
                if (valor%2==0) 
                {
                    pares=pares + 1;
                }
                else 
                {
                    impares=impares + 1;
                }
                x=x + 1;
            }
            Console.Write("Cantidad de pares:");
            Console.WriteLine(pares);
            Console.Write("Cantidad de impares:");
            Console.Write(impares);
            Console.ReadKey();
        }
    }
}
*/